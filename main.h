#ifndef __MAIN_H
#define __MAIN_H

#include <stdint.h>

typedef int32_t tcalc;

/*	Gyroscope measurements */
typedef struct
{
	tcalc incX;
	tcalc incY;
	tcalc incZ;
} AxisData;

/*	Orientation quaternion */
typedef struct
{
	tcalc q0;
	tcalc qX;
	tcalc qY;
	tcalc qZ;
} Quaternion;

#endif //__MAIN_H
