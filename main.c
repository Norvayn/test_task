/*
	Main testing routine
	
	Keil uVision doesn't support russian language comments, 
	so i will use simple english
*/

#include "main.h"
#include "quat_calc.h"

/*	Auxiliary float-format quaternion, only for testing purposes */
typedef struct
{
	float q0;
	float qX;
	float qY;
	float qZ;
} fQuaternion;

/*	Private function prototypes */
void CalculateFloatQuaternion(void);

/*	Private variables */
fQuaternion fQt;
Quaternion g_Qt = {0x7FFFFFFF, 0, 0, 0};

int32_t main(void)
{
/*
	Test case 1:
	
	g_Qt = {1, 0, 0, 0}
	Rotate by 45 degrees: 45 times by 1 degree (2880 * 1.25 = 3600'' = 1 deg)
*/
	
	AxisData aInc1 = {2880, 0, 0};
	
	for (uint8_t i = 0; i < 45; i++)
	{
		CalculateQuaternion(&aInc1);
	}
	
	CalculateFloatQuaternion();
	
/*
	Test case 2:
	
	g_Qt = {0.924, 0.383, 0, 0}
	Rotate by 45 degrees: 45 times by 1 degree (2880 * 1.25 = 3600'' = 1 deg)
*/
	
	AxisData aInc2 = {2880, 0, 0};
	
	for (uint8_t i = 0; i < 45; i++)
	{
		CalculateQuaternion(&aInc2);
	}
	
	CalculateFloatQuaternion();
	
/*
	Test case 3:
	
	g_Qt = {0.707, 0.707, 0, 0}
	Rotate by 90 degrees: 90 times by 1 degree (2880 * 1.25 = 3600'' = 1 deg)
*/
	
	AxisData aInc3 = {0, 2880, 0};
	
	for (uint8_t i = 0; i < 90; i++)
	{
		CalculateQuaternion(&aInc3);
	}
	
	CalculateFloatQuaternion();
	
/*
	Test case 4:
	
	g_Qt = {0.5, 0.5, 0.5, 0.5}
	Rotate by -45 degrees: 45 times by -1 degree (2880 * 1.25 = 3600'' = 1 deg)
*/
	
	AxisData aInc4 = {0, 0, -2880};
	
	for (uint8_t i = 0; i < 45; i++)
	{
		CalculateQuaternion(&aInc4);
	}
	
	CalculateFloatQuaternion();
	
/*
	End test:
	g_Qt = {0.653, 0.271, 0.653, 0.271}
*/
}

/*
	Calculate auxiliary float-format quaternion
*/
void CalculateFloatQuaternion()
{
	fQt.q0 = g_Qt.q0 / 2147483648.0f;
	fQt.qX = g_Qt.qX / 2147483648.0f;
	fQt.qY = g_Qt.qY / 2147483648.0f;
	fQt.qZ = g_Qt.qZ / 2147483648.0f;
}
