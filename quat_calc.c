/*
	Quaternion increment calculation module
	
	Keil uVision doesn't support russian language comments, 
	so i will use simple english
*/

#include "main.h"

/*	Uncomment this line to try my solution */
//#define MY_SOLUTION

/*	Custom Q2.30 type with range -1.(9) ... 1.(9) */
typedef int32_t		q30_t;

/*	Pre-build Q2.30 values for 1.0f and 1.5f */
#define Q30_1_5		0x60000000
#define Q30_1_0		0x40000000

/*	Private function prototypes */
q30_t inc_to_q30(int32_t value);
q30_t q30_mul(q30_t multiplicand, q30_t multiplier);
q30_t q30_inv_sqrt(q30_t value);

extern Quaternion g_Qt;

/*
	Gyroscope measurement to Q2.30 value conversion
	
	<value>: gyroscope measurement
	<return value>: Q2.30 value in radians
	
	Gyroscope has 1.25 ''/digit sensetivity
	
	Return value will have radian units, because this value will
	be used in cos and sin approximation
*/
q30_t inc_to_q30(int32_t value)
{
/*	1.25 / 3600 / 180 * pi * 2^30 */
	return (q30_t)(value * 0x196B);
}

/*
	Multiplication of two Q2.30 fixed point values
	
	<multiplicand>: Q2.30 value
	<multiplier>: Q2.30 value
	<return value>: Q2.30 value of their product
*/
q30_t q30_mul(q30_t multiplicand, q30_t multiplier)
{
	return (q30_t)((int64_t)multiplicand * multiplier >> 30);
}

/*
	Inverse square root of Q2.30 fixed point value
	
	<value>: Q2.30 value
	<return value>: Q2.30 value of inverse square root
	
	Here is Newton-Raphson method for successive approximation
*/
q30_t q30_inv_sqrt(q30_t value)
{
/*
	Squared sum of quaternion components always will be near 1.0f,
	so this is a good approximation for first round
*/
	q30_t sqrt = Q30_1_0;
	
/*	3 rounds of Newton-Raphson method */
	for (uint8_t i = 0; i < 3; i++)
	{
		sqrt = q30_mul(sqrt, Q30_1_5 - q30_mul(value, q30_mul(sqrt, sqrt >> 1)));
	}
	
	return sqrt;
}

/*
	Quaternion increment calculation
	
	<aInc>: gyroscope measurements structure
	
	This function will update externally defined quaternion variable
*/
void CalculateQuaternion(AxisData* aInc)
{
/*	Convert Q1.31 quaternion components into Q2.30 */
	q30_t QX = g_Qt.qX >> 1;
	q30_t QY = g_Qt.qY >> 1;
	q30_t QZ = g_Qt.qZ >> 1;
	q30_t Q0 = g_Qt.q0 >> 1;
	
/*	Calculate newly received gyroscope measurements as orientation quaternion*/
	
/*	These formulas were given in task description, and i don't quite understand them. */
	
	#ifndef MY_SOLUTION
	
	q30_t nX = inc_to_q30(aInc->incX) >> 1;
	q30_t nY = inc_to_q30(aInc->incY) >> 1;
	q30_t nZ = inc_to_q30(aInc->incZ) >> 1;
	q30_t n0 = Q30_1_0 - q30_mul(nX, nX) - q30_mul(nY, nY) - q30_mul(nZ, nZ);
	
	#else
	
/*	In my mind, this should be like this: */
	
	q30_t incX = inc_to_q30(aInc->incX);
    q30_t sinX = incX >> 1;
	q30_t cosX = Q30_1_0 - (q30_mul(incX, incX) >> 3);
	
	q30_t incY = inc_to_q30(aInc->incY);
	q30_t sinY = incY >> 1;
	q30_t cosY = Q30_1_0 - (q30_mul(incY, incY) >> 3);
	
	q30_t incZ = inc_to_q30(aInc->incZ);
	q30_t sinZ = incZ >> 1;
	q30_t cosZ = Q30_1_0 - (q30_mul(incZ, incZ) >> 3);
	
/*
	Now calculate approximate conversion of euler angles to quaternion:
	
	nx = sinx*cosy*cosz - cosx*siny*sinz
	ny = cosx*siny*cosz + sinx*cosy*sinz
	nz = cosx*cosy*sinz - sinx*siny*cosz
	n0 = cosx*cosy*cosz + sinx*siny*sinz
*/
	
	q30_t nX = q30_mul(q30_mul(sinX, cosY), cosZ) - q30_mul(q30_mul(cosX, sinY), sinZ);
	q30_t nY = q30_mul(q30_mul(cosX, sinY), cosZ) + q30_mul(q30_mul(sinX, cosY), sinZ);
	q30_t nZ = q30_mul(q30_mul(cosX, cosY), sinZ) - q30_mul(q30_mul(sinX, sinY), cosZ);
	q30_t n0 = q30_mul(q30_mul(cosX, cosY), cosZ) + q30_mul(q30_mul(sinX, sinY), sinZ);
	
	#endif
	
/*	Multiply two rotations in form of quaternions */
	q30_t qX = q30_mul(Q0, nX) + q30_mul(QX, n0) + q30_mul(QY, nZ) - q30_mul(QZ, nY);
	q30_t qY = q30_mul(Q0, nY) + q30_mul(QY, n0) + q30_mul(QZ, nX) - q30_mul(QX, nZ);
	q30_t qZ = q30_mul(Q0, nZ) + q30_mul(QZ, n0) + q30_mul(QX, nY) - q30_mul(QY, nX);
	q30_t q0 = q30_mul(Q0, n0) - q30_mul(QX, nX) - q30_mul(QY, nY) - q30_mul(QZ, nZ);
	
/*	Calculate inverse square root */
	q30_t q = q30_inv_sqrt(q30_mul(q0, q0) + q30_mul(qX, qX) + q30_mul(qY, qY) + q30_mul(qZ, qZ));
	
/*	Apply this value to newly computed quaternion and convert it back to Q1.31 format */
	g_Qt.qX = q30_mul(qX, q) << 1;
	g_Qt.qY = q30_mul(qY, q) << 1;
	g_Qt.qZ = q30_mul(qZ, q) << 1;
	g_Qt.q0 = q30_mul(q0, q) << 1;
}
